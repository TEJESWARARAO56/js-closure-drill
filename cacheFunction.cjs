function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if (arguments.length < 1 || typeof cb != 'function') {
        throw new Error("parameters not defined")
    }
    let cache = {};
    function memoize(...args) {
        if (args in cache) {
            console.log("from cache")
            return cache[args];
        }
        else {
            let result = cb(...args);
            cache[args] = result;
            return result;
        }
    }
    return memoize
}

module.exports = cacheFunction;

// function fibonacci(...m) {
//     let n = m[0]
//     if (n < 2) return n;
//     return fibonacci(n - 1) + fibonacci(n - 2);
// }

// let memoizedFibonacci = cacheFunction(fibonacci);
// console.log(memoizedFibonacci(10, 56, 78)); // Output: 55
// console.log(memoizedFibonacci(10,56 ,78)); // Output: 55
// console.log(memoizedFibonacci(11)); // output: 89
