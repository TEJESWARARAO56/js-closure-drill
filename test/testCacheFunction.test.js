let cacheFunction = require('../cacheFunction.cjs');

function fibonacci(...m) {
    let n = m[0]
    if (n < 2) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}
test("Testing cache function", () => {
    expect(cacheFunction(fibonacci)(10)).toStrictEqual(55)
})

