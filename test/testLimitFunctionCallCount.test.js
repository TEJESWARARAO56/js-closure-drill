let limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function cb(...args) {
    return args
}

test("Testing cache function", () => {
    let instance = limitFunctionCallCount(cb, 2)
    expect(instance(9, 23)).toStrictEqual([9, 23])
    expect(instance(9, 23)).toStrictEqual([9, 23])
    expect(instance(9, 23)).toStrictEqual(null)
})