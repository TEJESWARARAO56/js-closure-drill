let counterFactory = require('../counterFactory.cjs');

test("Testing cache function", () => {
    let counterFactoryInstance=counterFactory()
    expect(counterFactoryInstance.increment()).toStrictEqual(1)
    expect(counterFactoryInstance.increment()).toStrictEqual(2)
    expect(counterFactoryInstance.decrement()).toStrictEqual(1)
    expect(counterFactoryInstance.decrement()).toStrictEqual(0)
    expect(counterFactoryInstance.decrement()).toStrictEqual(-1)
})